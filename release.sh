#!/bin/bash

# Basic if statement
if [ $1 = 'i' ]
then
echo 'Releasing iOS...'
./appcenter-pre-build.sh && appcenter codepush release-react -a annie-elequin/Ditto-Messenger-iOS -d Staging
fi

if [ $1 = 'a' ]
then 
echo 'Releasing Android...'
./appcenter-pre-build.sh && appcenter codepush release-react -a annie-elequin/Ditto-Chat-Android -d Staging
fi