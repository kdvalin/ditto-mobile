import React from 'react';
import AppContainer from './src/AppContainer';
import {Provider} from 'react-redux';
import store from './src/redux/store';

var codePush = require('react-native-code-push');

// import * as Sentry from '@sentry/react-native';
// Sentry.init({
//   dsn: 'https://42044a4916614554874cdaf96ea70e59@sentry.io/1529001',
// });

const debug = require('debug');
debug.enable('ditto:*');

console.disableYellowBox = true;

const App = () => (
  <Provider store={store}>
    <AppContainer />
  </Provider>
);

const codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE
};
// export default codePush(codePushOptions)(App);
export default codePush(App);
