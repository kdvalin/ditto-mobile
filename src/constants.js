import {Dimensions} from 'react-native';
import Color from 'color';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

const placeholderTextColor = Color('#f1e6ff')
  .alpha(0.2)
  .rgb()
  .string();

export const COLORS = {
  dittoPurple: '#7202a3',
  lightBlue: '#b6def5',
  blue: '#0089ff',
  green: '#11af04',
  red: '#d0046a',
  orange: '#e06f0e',
  gray: {
    one: '#f1e6ff',
    two: '#b7a7c6',
    three: '#7c6c8b',
    four: '#53475f',
    five: '#3e3348'
  },
  dark: '#1d1630',
  headerColor: '#292142',
  placeholderTextColor
};

export const ROUTES = {
  rooms: {
    fetchPreviousMessages: (homeserver, token, roomId, lastToken = null) =>
      `${homeserver}/_matrix/client/r0/rooms/${roomId}/messages?access_token=${token}&dir=b${
        lastToken ? `&from=${lastToken}` : ''
      }`,
    avatar: (homeserver, token, roomId) =>
      `${homeserver}/_matrix/client/r0/rooms/${roomId}/state/m.room.avatar?access_token=${token}`,
    members: (homeserver, token, roomId) =>
      `${homeserver}/_matrix/client/r0/rooms/${roomId}/members?access_token=${token}`
  },
  users: {
    displayName: (homeserver, userId) =>
      `${homeserver}/_matrix/client/r0/profile/${userId}/displayname`
  }
};
