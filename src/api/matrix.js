import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {saveItemInCache, getItemFromCache} from '../utilities/Caching';
import {parse, stringify} from 'flatted/esm';
import {ROUTES} from '../constants';
import {formatMatrixEvent} from '../redux/actions/MatrixActions';

const debug = require('debug')('ditto:api:matrix');

export const fetchAvatarForRoom = async roomId => {
  const homeserver = await retrieveHomeserverFromCache();
  const userData = await retrieveUserDataFromCache();
  const url = ROUTES.rooms.avatar(homeserver, userData.accessToken, roomId);
  try {
    debug('fetchAvatarForRoom() url: ', url);
    const response = await axios.get(url);
    debug('fetchAvatarForRoom() response: ', response);
  } catch (error) {
    debug('ERROR fetching avatar for room: ', error);
    return null;
  }
};

export const getRoomTimeline = room => {
  if (!room) return [];
  const messages = room.getLiveTimeline().getEvents();
  const ret = [];
  messages.forEach(event => ret.push(formatMatrixEvent(event)));
  return ret;
};

export const fetchPreviousMessages = async (roomId, lastToken = null) => {
  const homeserver = await retrieveHomeserverFromCache();
  const userData = await retrieveUserDataFromCache();
  const url = ROUTES.rooms.fetchPreviousMessages(
    homeserver,
    userData.accessToken,
    roomId,
    lastToken
  );
  try {
    // debug('fetchPreviousMessages() url: ', url);
    const response = await axios.get(url);
    // debug('fetchPreviousMessages() response: ', response);
    return response;
  } catch (error) {
    debug('ERROR fetching previous messages: ', error);
    return null;
  }
};

export const getAllRoomMembers = async roomId => {
  const homeserver = await retrieveHomeserverFromCache();
  const userData = await retrieveUserDataFromCache();
  const url = ROUTES.rooms.members(homeserver, userData.accessToken, roomId);
  const response = await axios.get(url);
  return response.data.chunk;
};

export const getDisplayName = async userId => {
  const homeserver = await retrieveHomeserverFromCache();
  const url = ROUTES.users.displayName(homeserver, userId);
  debug('url thing', url);
  const response = await axios.get(url);
  debug('response', response);
  return response.data.displayname;
};

export const getAll = async requests => {
  const response = await axios.all(requests);
  return response;
};

export const saveUserDataToCache = data => {
  return saveItemInCache('userData', data);
};

export const retrieveUserDataFromCache = async () => {
  return await getItemFromCache('userData');
};

export const saveHomeserverToCache = homeserver => {
  return saveItemInCache('homeserver', homeserver);
};

export const retrieveHomeserverFromCache = async () => {
  return await getItemFromCache('homeserver');
};

export const saveCurrentRoomToCache = room => {
  return AsyncStorage.setItem('currentRoom', stringify(room));
};

export const retrieveCurrentRoomFromCache = async () => {
  const value = await AsyncStorage.getItem('currentRoom');
  return value ? parse(value) : null;
};

export const saveRoomListToCache = roomList => {
  return AsyncStorage.setItem('roomList', stringify(roomList));
};

export const retrieveRoomListFromCache = async () => {
  const value = await AsyncStorage.getItem('roomList');
  return parse(value);
};

export const saveMessageListToCache = (roomId, messageList) => {
  return AsyncStorage.setItem(`${roomId}messageList`, stringify(messageList));
};

export const retrieveMessageListFromCache = async roomId => {
  const value = await AsyncStorage.getItem(`${roomId}messageList`);
  return parse(value);
};

export const saveDisplayNameListToCache = nameList => {
  return AsyncStorage.setItem('nameList', stringify(nameList));
};

export const retrieveDisplayNameListFromCache = async () => {
  const value = await AsyncStorage.getItem('nameList');
  return value ? parse(value) : [];
};
