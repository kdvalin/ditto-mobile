import _ from 'lodash';
import sdk from 'matrix-js-sdk';
import {
  getDisplayName,
  retrieveHomeserverFromCache,
  retrieveUserDataFromCache,
  saveHomeserverToCache,
  saveUserDataToCache
} from '../../api/matrix';
import '../../poly';
import {clearCache, logCache} from '../../utilities/Caching';
import {
  clientSelector,
  currentMessagesSelector,
  currentRoomSelector,
  roomListSelector,
  userDataSelector
} from '../selectors';

const debug = require('debug')('ditto:redux:MatrixActions');

export const MatrixActionTypes = {
  SET_MATRIX_CLIENT: 'SET_MATRIX_CLIENT',
  SET_AUTH_ERROR: 'SET_AUTH_ERROR',
  CLEAR_AUTH_ERROR: 'CLEAR_AUTH_ERROR',
  SET_SYNC_ERROR: 'SET_SYNC_ERROR',
  UPDATE_IS_SYNCING: 'UPDATE_IS_SYNCING',
  UPDATE_NEW_MSG: 'UPDATE_NEW_MSG',
  SET_USER_DATA: 'SET_USER_DATA',
  SET_ROOM_LIST: 'SET_ROOM_LIST',
  SET_CURRENT_ROOM: 'SET_CURRENT_ROOM',
  SET_CURRENT_MESSAGES: 'SET_CURRENT_MESSAGES',
  USER_LOGOUT: 'USER_LOGOUT'
};

const setMatrixClient = client => ({
  type: MatrixActionTypes.SET_MATRIX_CLIENT,
  payload: client
});

const setAuthError = error => ({
  type: MatrixActionTypes.SET_AUTH_ERROR,
  payload: error
});

export const clearAuthError = () => ({
  type: MatrixActionTypes.CLEAR_AUTH_ERROR
});

export const setSyncError = error => ({
  type: MatrixActionTypes.SET_SYNC_ERROR,
  payload: error
});

const setUserData = data => ({
  type: MatrixActionTypes.SET_USER_DATA,
  payload: data
});

const setRoomList = roomList => ({
  type: MatrixActionTypes.SET_ROOM_LIST,
  payload: roomList
});

export const setCurrentRoom = room => {
  // saveCurrentRoomToCache(room);
  return {
    type: MatrixActionTypes.SET_CURRENT_ROOM,
    payload: room
  };
};

const setCurMessages = payload => ({
  type: MatrixActionTypes.SET_CURRENT_MESSAGES,
  payload
});

export const updateIsSyncing = value => ({
  type: MatrixActionTypes.UPDATE_IS_SYNCING,
  payload: value
});

export const updateNewMessage = value => ({
  type: MatrixActionTypes.UPDATE_NEW_MSG,
  payload: value
});

const userLogout = () => ({
  type: MatrixActionTypes.USER_LOGOUT
});

//********************************************************************************
// Thunks
//********************************************************************************

export const createMatrixClient = (
  username,
  password,
  homeserver = 'https://matrix.org'
) => {
  debug('createClientThunk()');
  return async (dispatch, getState) => {
    const client = sdk.createClient(homeserver);
    dispatch(setMatrixClient(client));
    saveHomeserverToCache(homeserver);
    client
      .login('m.login.password', {user: username, password})
      .then(({user_id, access_token}) => {
        const userData = {
          userId: user_id,
          accessToken: access_token,
          displayName: user_id
        };
        debug('user data', userData);
        saveUserDataToCache(userData);
        dispatch(setUserData(userData));
        dispatch(startMatrixClient());
      })
      .catch(err => {
        debug('Matrix Auth Error: ', err);
        dispatch(setAuthError(err));
      });
  };
};

const refreshClientFromToken = () => {
  return async (dispatch, getState) => {
    logCache();
    const {accessToken, userId} = await retrieveUserDataFromCache();
    const homeserver = await retrieveHomeserverFromCache();
    const client = sdk.createClient({
      baseUrl: homeserver,
      accessToken,
      userId
    });
    dispatch(setMatrixClient(client));
    dispatch(startMatrixClient());
  };
};

export const reloadFromCache = () => {
  debug('reloadFromCache()');
  return async (dispatch, getState) => {
    const userDataPromise = retrieveUserDataFromCache();
    const homeserverPromise = retrieveHomeserverFromCache();
    // const roomListPromise = retrieveRoomListFromCache();
    // const currentRoomPromise = retrieveCurrentRoomFromCache();

    dispatch(setUserData(await userDataPromise));
    // dispatch(setRoomList(await roomListPromise));
    // dispatch(setCurrentRoom(await currentRoomPromise));
  };
};

export const startMatrixClient = () => {
  debug('startMatrixClient()');
  return async (dispatch, getState) => {
    let client = clientSelector(getState());
    if (!client) {
      dispatch(refreshClientFromToken());
      return;
    }
    debug('we have a client');

    client.on('sync', (state, prevState, data) =>
      onSync(state, prevState, data, dispatch, getState, client)
    );

    client.on('Room.timeline', (data, room) =>
      onTimeline(data, room, dispatch, getState, client)
    );

    const opts = {
      initialSyncLimit: 4,
      lazyLoadMembers: true
    };
    client.startClient(opts);
  };
};

const onSync = async (state, prevState, data, dispatch, getState, client) => {
  try {
    debug('SYNCING STATE', state);
    const roomList = client.getRooms();
    switch (state) {
      case 'PREPARED':
        debug('room list', roomList);
        const sortedPreparedRooms = sortRooms(roomList);
        // debug('FIRST ROOM: ', roomList[0].getLiveTimeline());
        // debug('FIRST ROOM: ', sortedPreparedRooms[0].getLiveTimeline());
        // saveRoomListToCache(sortedPreparedRooms);
        dispatch(setRoomList(sortedPreparedRooms));
        // const currentRoom = await retrieveCurrentRoomFromCache();
        // if (!currentRoom) dispatch(setCurrentRoom(sortedPreparedRooms[0]));
        break;
      case 'SYNCING':
        debug('STATE: ', data);
        dispatch(updateIsSyncing(false));
        const curRoomList = roomListSelector(getState());
        if (!_.isEqual(roomList, curRoomList)) {
          const sortedSyncedRooms = sortRooms(roomList);
          dispatch(setRoomList(sortedSyncedRooms));
        }
        break;
      case 'ERROR':
        debug('Sync Error: ', data.error);
        dispatch(setSyncError(data.error));
        break;
    }
  } catch (error) {
    console.warn('error syncing', error);
  }
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const client = clientSelector(getState());
    client.stopClient();
    dispatch(setMatrixClient(null));
    dispatch(setRoomList([]));
    dispatch(setUserData(null));
    dispatch(userLogout());
    clearCache();
  };
};

const onTimeline = (data, room, dispatch, getState, client) => {
  const currentRoom = currentRoomSelector(getState());
  if (room.roomId !== currentRoom?.roomId) return;
  // debug('onTimeline()', data);
  const isUser = userDataSelector(getState()).userId === data.getSender();
  // debug('sender', data.getSender());
  if (!isUser) {
    const messages = [
      formatMatrixEvent(data),
      ...currentMessagesSelector(getState())
    ];
    const newMessages = _.uniqWith(
      messages,
      (a, b) => a.event_id === b.event_id
    );
    dispatch(setCurrentMessages(newMessages));
  }
  // debug('room', room);
  // const newRoomList = [...roomListSelector(getState())];
  // const roomIndex = newRoomList.findIndex(r => r.roomId === room.roomId);
  // newRoomList.splice(roomIndex, 1, room);
  // dispatch(setRoomList(newRoomList));
  // debug('data', data);
  // if (
  //   data?.event?.type === 'm.room.message' &&
  //   data?.sender?.roomId === currentRoomSelector(getState())?.roomId
  // ) {
  //   dispatch(updateNewMessage(true));
  // }
};

export const setCurrentMessages = messages => {
  debug('setCurrentMessages()');
  return async dispatch => {
    const messageList = removeDuplicateEvents(messages);
    dispatch(setCurMessages(messageList));

    // const nameList = await retrieveDisplayNameListFromCache();
    // try {
    //   const response = await updateDisplayNames(messageList, nameList);
    //   debug('response???', response);
    // } catch (error) {
    //   debug("well that didn't work", error);
    // }
    // dispatch(setCurMessages(messageList));
    // saveDisplayNameListToCache(nameList);
  };
};

const updateDisplayNames = (messageList, nameList) => {
  return new Promise(async (resolve, reject) => {
    debug('messageList', messageList);
    for (let i = 0; i < messageList.length; i++) {
      debug('boop', messageList[i]);
      const msg = messageList[i];
      if (nameList[msg.sender]) {
        msg['sender_name'] = nameList[msg.sender];
      } else {
        try {
          const displayName = await getDisplayName(msg.sender);
          nameList[msg.sender] = displayName;
          msg['sender_name'] = nameList[msg.sender];
        } catch (error) {
          debug("Couldn't get display name: ", error);
        }
      }
      messageList[i] = msg;
    }
    resolve({messageList, nameList});
  });
};

const sortRooms = rooms => {
  return rooms.sort((a, b) => {
    const timelineA = a.getLiveTimeline().getEvents();
    const timelineB = b.getLiveTimeline().getEvents();

    const lastEventA = timelineA[timelineA.length - 1];
    const lastEventB = timelineB[timelineB.length - 1];

    const timestampA = lastEventA.getTs();
    const timestampB = lastEventB.getTs();

    return timestampB - timestampA;
  });
};

export const formatMatrixEvent = event => {
  const ret = {};
  try {
    ret['age'] = event.getAge();
    ret['content'] = event.getContent();
    ret['event_id'] = event.getId();
    ret['origin_server_ts'] = event.event.origin_server_ts;
    ret['room_id'] = event.getRoomId();
    ret['sender'] = event.getSender();
    ret['type'] = event.getType();
    return ret;
  } catch (error) {
    // debug('Could not format event: ', event);
    return event;
  }
};

export const removeDuplicateEvents = events => {
  const ret = _.uniqWith(events, (a, b) => a.event_id === b.event_id);
  return ret;
};
