export const authErrorSelector = state => state.matrix.authError;
export const userDataSelector = state => state.matrix.userData;
export const clientSelector = state => state.matrix.client;
export const roomListSelector = state => state.matrix.roomList;
export const currentRoomSelector = state => state.matrix.currentRoom;
export const currentMessagesSelector = state => state.matrix.currentMessages;
export const newMessageSelector = state => state.matrix.newMessage;
export const isSyncingSelector = state => state.matrix.isSyncing;
