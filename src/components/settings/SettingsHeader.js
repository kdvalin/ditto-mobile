import {COLORS} from '../../constants';
import ExitIcon from '../../assets/icons/icon-exit.svg';
import React from 'react';
import Text from '../library/Text';
import {View} from 'react-native';
import styled from 'styled-components/native';

const SettingsHeader = ({
  scene: {
    descriptor: {title}
  },
  navigation
}) => {
  const navigateHome = () => {
    navigation.navigate('Home');
  };

  return (
    <SettingsWrapper>
      <View style={{width: 60, backgroundColor: 'blue'}} />
      <Title>Settings</Title>
      <BackButtonWrapper onPress={navigateHome}>
        <ExitIcon width={20} height={20} fill={COLORS.gray.one} />
      </BackButtonWrapper>
    </SettingsWrapper>
  );
};

const SettingsWrapper = styled.SafeAreaView`
  background-color: ${COLORS.dark};
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Title = styled(Text)`
  font-weight: 600;
  font-size: 24;
`;

const BackButtonWrapper = styled.TouchableOpacity`
  margin-right: 20;
`;

export default SettingsHeader;
