import React, {Component} from 'react';
import {Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import styled from 'styled-components/native';
import {COLORS} from '../../constants';
import { withNavigation } from 'react-navigation';

class HeaderRight extends Component {
  notify = () => {
    this.props.navigation.navigate('CreateMessage')
  };
  render() {
    return (
      <Wrapper onPress={this.notify}>
        <Plus name='plus' size={25} color={COLORS.gray.one} />
      </Wrapper>
    );
  }
}
export default withNavigation(HeaderRight);

const Wrapper = styled.TouchableOpacity`
  background-color: ${COLORS.red};
  margin-right: 14;
  border-radius: 30;
  width: 30;
  height: 30;
  justify-content: center;
  align-items: center;
`;

const Plus = styled(Icon)`
  width: 25;
  height: 25;
`;
