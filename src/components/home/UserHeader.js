import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Alert} from 'react-native';
import styled from 'styled-components/native';
import {clientSelector, userDataSelector} from '../../redux/selectors';
import {
  retrieveHomeserverFromCache,
  retrieveUserDataFromCache,
  saveUserDataToCache
} from '../../api/matrix';
import {logoutUser} from '../../redux/actions/MatrixActions';
import {withNavigation} from 'react-navigation';
import codePush from 'react-native-code-push';

const debug = require('debug')('ditto:component:UserHeader');

class UserHeader extends Component {
  state = {
    displayName: '',
    avatarUrl: '',
    codePushVersion: ''
  };

  componentDidMount = async () => {
    const {displayName, avatarUrl} = await retrieveUserDataFromCache();
    this.setState({displayName, avatarUrl});
    this.setDisplayNameAndAvatar();

    codePush.getUpdateMetadata().then(metadata => {
      debug('META DATA', metadata);
      this.setState({
        codePushVersion: `${metadata.appVersion} - ${metadata.label}`
      });
    });
  };

  componentDidUpdate = prevProps => {
    const {client, userId} = this.props;
    if (client !== prevProps.client || userId !== prevProps.userId) {
      this.setDisplayNameAndAvatar();
    }
  };

  setDisplayNameAndAvatar = async () => {
    const {client, userId} = this.props;
    debug('setDisplayNameAndAvatar()', this.props);
    if (!client) return;
    const {displayname} = await client.getProfileInfo(userId, 'displayname');
    const avatarData = await client.getProfileInfo(userId, 'avatar_url');
    debug('avatarData', avatarData);
    const avatarUrl = client.mxcUrlToHttp(
      avatarData.avatar_url,
      150,
      150,
      'crop'
    );
    if (
      this.state.displayName !== displayname ||
      this.state.avatarUrl !== avatarUrl
    ) {
      this.setState({displayName: displayname, avatarUrl});
      this.updateCacheUserData(displayname, avatarUrl);
    }
  };

  updateCacheUserData = async (displayName, avatarUrl) => {
    const userData = await retrieveUserDataFromCache();
    userData['displayName'] = displayName;
    userData['avatarUrl'] = avatarUrl;
    saveUserDataToCache(userData);
  };

  notify = () => {
    Alert.alert(
      'Settings',
      `${this.state.codePushVersion}\n\nIn the future this will take you to a settings screen.`,
      [
        {
          text: 'Cancel',
          style: 'default'
        },
        {text: 'LOGOUT', onPress: this.logoutUser, style: 'destructive'}
      ],
      {cancelable: false}
    );
  };

  navToSettingsScreen = () => {
    this.props.navigation.navigate('Settings');
  };

  logoutUser = () => {
    this.props.logoutUser();
    this.props.navigation.navigate('Auth');
  };

  render() {
    return (
      <Wrapper onPress={this.navToSettingsScreen}>
        <Avatar source={{uri: this.state.avatarUrl}} />
        <Name>{this.state.displayName}</Name>
      </Wrapper>
    );
  }
}
const mapStateToProps = state => ({
  client: clientSelector(state),
  userId: userDataSelector(state)?.userId
});
const mapDispatchToProps = {
  logoutUser
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(UserHeader));

const Wrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-left: 14;
`;

const Avatar = styled.Image`
  width: 35;
  height: 35;
  background-color: gray;
  border-radius: 80;
`;

const Name = styled.Text`
  color: #fff;
  font-weight: bold;
  font-size: 18;
  margin-left: 14;
`;
