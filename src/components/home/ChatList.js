import React, {Component} from 'react';
import {FlatList} from 'react-native';
import {connect} from 'react-redux';
import {EventTimeline} from 'matrix-js-sdk';
import {roomListSelector, isSyncingSelector} from '../../redux/selectors';
import {getChatSnippet} from '../../utilities/Messages';
import {
  ChatListItem,
  Avatar,
  ChatTitle,
  Title,
  Snippet
} from './ChatListComponents';
import {withNavigation} from 'react-navigation';
import {setCurrentRoom} from '../../redux/actions/MatrixActions';
import styled from 'styled-components/native';
import {COLORS} from '../../constants';
import {fetchAvatarForRoom, retrieveHomeserverFromCache} from '../../api/matrix';
import { isEqual } from 'lodash';

const debug = require('debug')('ditto:component:ChatList');

class ChatList extends Component {

  state = {
    avatarUrls: {},
    chatList: []
  }

  navigateToChat = room => {
    this.props.setCurrentRoom(room);
    this.props.navigation.navigate('Chat', {room});
  };

  getAvatarsForRooms = async () => {
    const avatarUrls = {};
    const homeserver = await retrieveHomeserverFromCache();
    this.state.chatList.forEach(room => {
      const url = room.getAvatarUrl(homeserver, 50, 50, 'crop');
      avatarUrls[room.roomId] = url;
    });
    this.setState({ avatarUrls });
  }

  updateChatList = () => {
    const directChats = this.props.rooms.filter(room => {
      const state = room.getLiveTimeline().getState(EventTimeline.FORWARDS);
      const numMembers = Object.keys(state.members).length;
      return this.props.groups ? numMembers >= 2 : numMembers === 2;
    });
    const groupChats = this.props.rooms.filter(room => {
      const state = room.getLiveTimeline().getState(EventTimeline.FORWARDS);
      const numMembers = Object.keys(state.members).length;
      // debug('room name', room.name);
      // debug('num members', numMembers);
      return numMembers > 2;
    });
    const chatList = this.props.groups ? groupChats : directChats;
    this.setState({ chatList }, () => this.getAvatarsForRooms());
  }

  componentDidMount = () => {
    // if (this.props.rooms.length > 0) {
    //   const events = this.props.rooms[0]
    //     .getLiveTimeline()
    //     .getEvents(EventTimeline.BACKWARDS);
    //   // debug('events', events);
    // }
    this.updateChatList();
  }

  componentDidUpdate = prevProps => {
    if (!isEqual(prevProps.rooms, this.props.rooms)) {
      this.updateChatList();
    }
  }

  render() {
    return (
      <>
        {this.props.isSyncing && (
          <SyncingIndicator>
            <SyncingText>Syncing...</SyncingText>
          </SyncingIndicator>
        )}
        <FlatList
          data={this.state.chatList}
          renderItem={({item: room}) => {
            return (
              <ChatListItem
                key={room.roomId}
                onPress={() => this.navigateToChat(room)}>
                <Avatar source={{uri: this.state.avatarUrls[room.roomId]}} />
                <ChatTitle>
                  <Title numberOfLines={1}>{room.name}</Title>
                  <Snippet numberOfLines={1}>{getChatSnippet(room)}</Snippet>
                </ChatTitle>
              </ChatListItem>
            );
          }}
          keyExtractor={item => item.roomId}
        />
      </>
    );
  }
}
const mapStateToProps = state => ({
  rooms: roomListSelector(state),
  isSyncing: isSyncingSelector(state)
});
const mapDispatch = {
  setCurrentRoom
};
export default connect(mapStateToProps, mapDispatch)(withNavigation(ChatList));

const SyncingIndicator = styled.View`
  background-color: #4e22be;
  height: 20;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const SyncingText = styled.Text`
  color: ${COLORS.gray.one};
  font-style: italic;
  font-size: 12;
`;
