import React, {Component} from 'react';
import {currentRoomSelector, userDataSelector} from '../../../redux/selectors';
import {getNameColor, isEmoji} from '../../../utilities/Misc';

import {COLORS} from '../../../constants';
import HTML from 'react-native-render-html';
import {connect} from 'react-redux';
import styled from 'styled-components/native';

const debug = require('debug')('ditto:component:MessageBubble');

class TextMessageBubble extends Component {
  onLinkPress = url => {
    debug('url', url);
  };

  render() {
    const {userData, message, prevMessage, nextMessage} = this.props;

    const sender = message.sender;
    const prevSenderSame = prevMessage && prevMessage.sender === sender;
    const nextSenderSame = nextMessage && nextMessage.sender === sender;

    const props = {
      prevSenderSame,
      nextSenderSame
    };

    let displayText = message.content.formatted_body || message.content.body;

    const senderName =
      this.props.displayNames[message.sender] || message.sender;

    let reactions = [];
    if (
      message.unsigned &&
      message.unsigned['m.relations'] &&
      message.unsigned['m.relations']['m.annotation']?.chunk?.length > 0
    ) {
      message.unsigned['m.relations']['m.annotation'].chunk.forEach(r => {
        reactions.push(r.key);
      });
      debug('reactions', reactions);
    }

    if (userData.userId === sender) {
      return (
        <>
          {isEmoji(displayText) ? (
            <Emoji isUser={true} {...props}>
              {displayText}
            </Emoji>
          ) : (
            <MyBubble {...props}>
              <HTML html={displayText} {...htmlProps} />
            </MyBubble>
          )}

          {!prevSenderSame && (
            <SenderText isUser={true} color={getNameColor(message.sender)}>
              {senderName}
            </SenderText>
          )}
        </>
      );
    } else {
      return (
        <>
          {isEmoji(displayText) ? (
            <Emoji {...props}>{displayText}</Emoji>
          ) : (
            <OtherBubble {...props}>
              <HTML html={displayText} {...htmlProps} />
            </OtherBubble>
          )}

          {!prevSenderSame && (
            <SenderText color={getNameColor(message.sender)}>
              {senderName}
            </SenderText>
          )}
        </>
      );
    }
  }
}
const mapStateToProps = state => ({
  userData: userDataSelector(state),
  currentRoom: currentRoomSelector(state)
});
export default connect(mapStateToProps, null)(TextMessageBubble);

const baseFontStyle = {
  color: COLORS.gray.one,
  fontSize: 16,
  letterSpacing: 0.3,
  fontWeight: '400'
};

const tagsStyles = {
  blockquote: {
    borderLeftColor: COLORS.red,
    borderLeftWidth: 3,
    paddingLeft: 10,
    marginVertical: 10,
    opacity: 0.8
  },
  p: {
    marginVertical: 6
  }
};

const htmlProps = {
  baseFontStyle,
  tagsStyles
};

// const markdownStyles = {
//   root: {
//     color: COLORS.gray.one,
//     fontSize: 16,
//     letterSpacing: 0.3,
//     fontWeight: '500'
//   },
//   blockquote: {
//     borderLeftWidth: 3,
//     borderLeftColor: COLORS.red,
//     paddingLeft: 10,
//     marginTop: 10,
//     marginBottom: 10
//   },
//   codeBlock: {
//     backgroundColor: COLORS.dark,
//     padding: 10,
//     fontFamily: 'Courier',
//     marginVertical: 10,
//     marginRight: 10
//   },
//   codeInline: {
//     backgroundColor: COLORS.dark,
//     padding: 10,
//     fontFamily: 'Courier',
//     lineHeight: 30
//   },
//   link: {
//     color: COLORS.red,
//     textDecorationLine: 'underline',
//     fontWeight: '500',
//     letterSpacing: 0.3
//   }
// };

// const Reactions = styled.Text`
//   z-index: 1;
//   font-size: 20;
//   align-self: flex-end;
// `;

const Emoji = styled.Text`
  font-size: 45;
  margin-left: 8;
  margin-right: 8;
  margin-top: 4;
  margin-bottom: 4;
  align-self: ${({isUser}) => (isUser ? 'flex-end' : 'flex-start')};
`;

const Bubble = styled.View`
  padding-left: 16;
  padding-right: 16;
  padding-top: 8;
  padding-bottom: 8;

  margin-top: ${({prevSenderSame}) => (prevSenderSame ? 2 : 2)};
  margin-bottom: ${({nextSenderSame}) => (nextSenderSame ? 1 : 4)};
  margin-left: 8;
  margin-right: 8;

  max-width: 300;

  border-radius: 18;
`;

const sharpBorderRadius = 5;

const OtherBubble = styled(Bubble)`
  background-color: red;
  align-self: flex-start;
  background-color: ${COLORS.headerColor};

  ${({prevSenderSame}) =>
    prevSenderSame ? `border-top-left-radius: ${sharpBorderRadius};` : ''}
  ${({nextSenderSame}) =>
    nextSenderSame ? `border-bottom-left-radius: ${sharpBorderRadius};` : ''}
`;

const MyBubble = styled(Bubble)`
  background-color: blue;
  align-self: flex-end;
  background-color: ${COLORS.dittoPurple};

  ${({prevSenderSame}) =>
    prevSenderSame ? `border-top-right-radius: ${sharpBorderRadius};` : ''}
  ${({nextSenderSame}) =>
    nextSenderSame ? `border-bottom-right-radius: ${sharpBorderRadius};` : ''}
`;

const SenderText = styled.Text`
  color: ${({color}) => (color ? color : 'pink')};
  font-size: 14;
  font-weight: 400;
  margin-left: 22;
  margin-right: 22;
  margin-top: 8;
  opacity: 0.8;
  ${({isUser}) => (isUser ? 'text-align: right;' : '')};
`;
