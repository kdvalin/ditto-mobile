import React, {Component} from 'react';
import {Text, View} from 'react-native';

import Image from 'react-native-scalable-image';
import {connect} from 'react-redux';
import {getNameColor} from '../../../utilities/Misc';
import styled from 'styled-components/native';
import {userDataSelector} from '../../../redux/selectors';

const debug = require('debug')('ditto:component:ImageMessageBubble');

class ImageMessageBubble extends Component {
  render() {
    const {
      message: {
        content: {url}
      }
    } = this.props;
    const imageUrl = this.props.client.mxcUrlToHttp(url);
    debug('this.props.message', this.props.message);

    const {userData, message, prevMessage, nextMessage} = this.props;

    const sender = message.sender;
    const prevSenderSame = prevMessage && prevMessage.sender === sender;
    const nextSenderSame = nextMessage && nextMessage.sender === sender;

    const senderName =
      this.props.displayNames[message.sender] || message.sender;

    return (
      <>
        <ImageWrapper
          myMessage={userData.userId === message.sender}
          prevSenderSame={prevSenderSame}
          nextSenderSame={nextSenderSame}>
          <Image
            source={{uri: imageUrl}}
            width={250}
            style={{borderRadius: 20, minWidth: 250, minHeight: 180}}
            defaultSource={require('../../../assets/images/placeholder.png')}
          />
        </ImageWrapper>
        {!prevSenderSame && (
          <SenderText
            isUser={userData.userId === message.sender}
            color={getNameColor(message.sender)}>
            {senderName}
          </SenderText>
        )}
      </>
    );
  }
}
const mapState = state => ({
  userData: userDataSelector(state)
});
export default connect(mapState, null)(ImageMessageBubble);

const ImageWrapper = styled.View`
  align-self: ${({myMessage}) => (myMessage ? 'flex-end' : 'flex-start')};
  margin-left: 12;
  margin-right: 12;
  margin-top: 2;
  margin-bottom: ${({nextSenderSame}) => (nextSenderSame ? 1 : 4)};
`;

const SenderText = styled.Text`
  color: ${({color}) => (color ? color : 'pink')};
  font-size: 14;
  font-weight: 400;
  margin-left: 22;
  margin-right: 22;
  margin-top: 8;
  opacity: 0.8;
  ${({isUser}) => (isUser ? 'text-align: right;' : '')};
`;
