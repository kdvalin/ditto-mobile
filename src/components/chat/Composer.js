import {KeyboardAvoidingView, SafeAreaView, Text, View} from 'react-native';
import React, {Component} from 'react';

import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import {COLORS} from '../../constants';
import ChevronsUp from '../../assets/icons/icon-chevrons-up.svg';
import IconPlus from '../../assets/icons/icon-plus.svg';
import styled from 'styled-components/native';

export default class Composer extends Component {
  state = {
    messageValue: ''
  };

  onSend = () => {
    this.props.onSend(this.state.messageValue);
    this.setState({messageValue: ''});
  };

  render() {
    return (
      <Wrapper>
        <ActionButton style={{padding: 10}} onPress={this.props.onImagePick}>
          <IconPlus fill={COLORS.gray.one} width={16} height={16} />
        </ActionButton>
        <GrowingTextInput
          placeholder='Type a message...'
          placeholderTextColor='rgba(255,255,255,.3)'
          value={this.state.messageValue}
          onChangeText={messageValue => this.setState({messageValue})}
        />
        <ActionButton style={{padding: 10}} onPress={this.onSend}>
          <ChevronsUp fill={COLORS.gray.one} width={16} height={16} />
        </ActionButton>
      </Wrapper>
    );
  }
}

const Wrapper = styled.SafeAreaView`
  flex-direction: row;
  margin-top: 20;
`;

const GrowingTextInput = styled(AutoGrowingTextInput)`
  background-color: ${COLORS.headerColor};
  flex: 1;
  border-radius: 20;
  padding-top: 10;
  padding-bottom: 10;
  padding-left: 14;
  padding-right: 14;
  color: ${COLORS.gray.one};
  font-size: 16;
  letter-spacing: 0.3;
  font-weight: 500;
`;

const ActionButton = styled.TouchableOpacity`
  background-color: ${COLORS.red};
  justify-content: center;
  align-items: center;
  border-radius: 40;
  margin-left: 8;
  margin-right: 8;
  width: 35;
  height: 35;
  align-self: flex-end;
`;
