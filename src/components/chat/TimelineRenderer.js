import React, {Component} from 'react';

import {Image} from 'react-native';
import ImageMessageBubble from './MessageTypes/ImageMessageBubble';
import TextMessageBubble from './MessageTypes/TextMessageBubble';
import {clientSelector} from '../../redux/selectors';
import {connect} from 'react-redux';
import styled from 'styled-components/native';

const debug = require('debug')('ditto:component:MessageBubble');

/**
 * TODO:
 * m.notice
 * m.video
 * m.relates_to (reactions)
 */

class TimelineRenderer extends Component {
  renderEvent = () => {
    switch (this.props.message.type) {
      case 'm.room.message':
        return this.renderMessage();
      case 'm.room.member':
        return this.renderMembership();
      case 'm.room.avatar':
        const {message, displayNames} = this.props;
        return (
          <SystemMessage>
            {displayNames[message.sender] || message.sender} changed the room
            avatar.
          </SystemMessage>
        );
      default:
        debug('message type', this.props.message);
        return (
          <SystemMessage>
            Ditto does not support the type "{this.props.message.type}" yet.
          </SystemMessage>
        );
    }
  };

  renderMessage = () => {
    switch (this.props.message.content.msgtype) {
      case 'm.text':
        return <TextMessageBubble {...this.props} />;
      case 'm.image':
        return <ImageMessageBubble {...this.props} />;
      default:
        debug('message content type', this.props.message);
        return (
          <SystemMessage>
            Ditto does not support the message type "
            {this.props.message.content.msgtype}" yet.
          </SystemMessage>
        );
    }
  };

  renderMembership = () => {
    switch (this.props.message.content.membership) {
      default:
        return (
          <SystemMessage>
            Ditto does not support the membership type "
            {this.props.message.content.membership}" yet.
          </SystemMessage>
        );
    }
  };

  render() {
    return this.renderEvent();
  }
}

const mapStateToProps = state => ({
  client: clientSelector(state)
});
export default connect(mapStateToProps)(TimelineRenderer);

const SystemMessage = styled.Text`
  color: rgba(255, 255, 255, 0.3);
  font-size: 12;
  align-self: center;
  margin-top: 8;
  margin-bottom: 4;
`;
