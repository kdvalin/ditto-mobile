import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';

import AuthLoadingScreen from './screens/auth/AuthLoadingScreen';
import LandingScreen from './screens/auth/LandingScreen';
import LoginScreen from './screens/auth/LoginScreen';
import SignUpOneScreen from './screens/auth/SignUpOneScreen';
import SignUpTwoScreen from './screens/auth/SignUpTwoScreen';
// import MainScreen from './screens/main/MainScreen';
// import SettingsScreen fro./screens/main/SettingsScreen-unusedeen';
// import HomeScreen from './screens/main/HomeScreen';
import ChatScreen from './screens/main/ChatScreen';
import DirectMessageScreen from './screens/main/home/DirectMessageScreen';
import GroupMessageScreen from './screens/main/home/GroupMessageScreen';
import UserHeader from './components/home/UserHeader';
import HeaderRight from './components/home/HeaderRight';
import SettingsHeader from './components/settings/SettingsHeader';
import {COLORS} from './constants';
import {isIphoneX} from 'react-native-iphone-x-helper';
import SettingsScreen from './screens/main/SettingsScreen';
import CreateNewMessageModal from './screens/main/CreateNewMessageModal';

const HomeSwipeStack = createMaterialTopTabNavigator(
  {
    DirectMessages: {
      screen: DirectMessageScreen,
      navigationOptions: {
        tabBarLabel: 'Messages'
      }
    },
    GroupMessages: {
      screen: GroupMessageScreen,
      navigationOptions: {
        tabBarLabel: 'Groups'
      }
    }
  },
  {
    initialRouteName: 'DirectMessages',
    tabBarPosition: 'bottom',
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: COLORS.gray.one,
      inactiveTintColor: '#898593',
      style: {
        backgroundColor: COLORS.headerColor,
        borderTopWidth: 0.4,
        borderTopColor: '#2F2646',
        height: isIphoneX() ? 75 : undefined,
        paddingLeft: 40,
        paddingRight: 40
      },
      indicatorStyle: {
        height: 0
      },
      tabStyle: {
        alignSelf: 'center',
        padding: 0
      },
      labelStyle: {
        fontWeight: 'bold',
        fontSize: 22
      },
      upperCaseLabel: false
    }
  }
);

const appHeaderStyle = {
  backgroundColor: COLORS.headerColor,
  borderBottomWidth: 0.4,
  borderBottomColor: '#2F2646'
};

const AppStack = createStackNavigator(
  {
    Home: HomeSwipeStack,
    Chat: ChatScreen
  },
  {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
      headerStyle: appHeaderStyle,
      headerLeft: <UserHeader />,
      headerRight: <HeaderRight />
    }
  }
);

const SettingsScreenWithHeader = createStackNavigator(
  {SettingsScreen},
  {
    defaultNavigationOptions: {
      headerMode: 'screen',
      header: SettingsHeader,
      headerTitle: 'Settings'
    }
  }
);
const CreateMessageModalWithHeader = createStackNavigator(
  {CreateNewMessageModal},
  {defaultNavigationOptions: {headerStyle: appHeaderStyle}}
);

const AppStackWithModals = createStackNavigator(
  {
    Main: AppStack,
    Settings: SettingsScreenWithHeader,
    CreateMessage: CreateMessageModalWithHeader
  },
  {
    initialRouteName: 'Main',
    mode: 'modal',
    headerMode: 'none'
  }
);

const AuthStack = createStackNavigator(
  {
    Landing: LandingScreen,
    SignUpOne: SignUpOneScreen,
    SignUpTwo: SignUpTwoScreen,
    Login: LoginScreen
  },
  {
    initialRouteName: 'Landing',
    headerMode: 'none'
  }
);

const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthStack,
    App: AppStackWithModals
  },
  {
    initialRouteName: 'AuthLoading',
    headerMode: 'none',
    navigationOptions: {
      gesturesEnabled: false
    }
  }
);

export default createAppContainer(AppNavigator);
