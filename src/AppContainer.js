import React, {Component} from 'react';
import AppNavigator from './AppNavigator';
import {ThemeProvider} from 'styled-components';
import {connect} from 'react-redux';
import theme from './theme';

const debug = require('debug')('ditto:AppContainer');

class AppContainer extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <AppNavigator />
      </ThemeProvider>
    );
  }
}
export default AppContainer;
