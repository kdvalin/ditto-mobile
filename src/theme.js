import {COLORS} from './constants';

const theme = {
  backgroundColor: COLORS.dark,
  dittoWhite: COLORS.gray.one,
};
export default theme;
