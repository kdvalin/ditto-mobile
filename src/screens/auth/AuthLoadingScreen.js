import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View, Text} from 'react-native';
import {retrieveUserDataFromCache} from '../../api/matrix';
import {
  startMatrixClient,
  reloadFromCache
} from '../../redux/actions/MatrixActions';
import styled from 'styled-components/native';
import {COLORS} from '../../constants';
import {
  startupNotifications,
  shutdownNotifications
} from '../../utilities/Notifications';

const debug = require('debug')('ditto:screen:AuthLoadingScreen');

class AuthLoadingScreen extends Component {
  componentDidMount = async () => {
    const userData = await retrieveUserDataFromCache();
    debug('User data: ', userData);
    if (userData) {
      startupNotifications();
      this.props.reloadFromCache();
      this.props.startMatrixClient();
      this.props.navigation.navigate('App');
    } else {
      this.props.navigation.navigate('Auth');
    }
  };

  componentWillUnmount = () => {
    shutdownNotifications();
  };

  render() {
    return <Screen />;
  }
}
const mapDispatchToProps = {
  startMatrixClient,
  reloadFromCache
};
export default connect(null, mapDispatchToProps)(AuthLoadingScreen);

const Screen = styled.View`
  flex: 1;
  background-color: ${COLORS.dark};
`;
