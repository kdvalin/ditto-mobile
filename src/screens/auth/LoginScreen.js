import React, {Component} from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';
import styled from 'styled-components/native';
import PageWrapper from '../../components/library/PageWrapper';
import Color from 'color';
import {COLORS, SCREEN_WIDTH, SCREEN_HEIGHT} from '../../constants';
import Button from '../../components/library/Button';
import AuthFooter from '../../components/auth/AuthFooter';
// import DittoIcon from '../../assets/icons/DittoIcon';
import Input from '../../components/library/Input';
import Image from 'react-native-scalable-image';
import Text from '../../components/library/Text';
import {
  createMatrixClient,
  clearAuthError
} from '../../redux/actions/MatrixActions';
import {authErrorSelector, userDataSelector} from '../../redux/selectors';
import BackIcon from '../../components/library/BackIcon';

const topBlob = require('../../assets/images/blob4.png');
const bottomBlob = require('../../assets/images/blob5.png');

const debug = require('debug')('ditto:screen:LoginScreen');

type IProps = {
  authError: any,
  clearAuthError: () => void,
  createMatrixClient: (
    username: string,
    password: string,
    homeserver?: string
  ) => void
};

type IState = {
  usernameValue: string,
  passwordValue: string,
  homeserverValue: string,
  isLoading: boolean
};

class LoginScreen extends Component<IProps, IState> {
  passwordInput = null;

  state = {
    usernameValue: '',
    passwordValue: '',
    homeserverValue: '',
    isLoading: false
  };

  componentDidUpdate = prevProps => {
    if (!prevProps.userData && this.props.userData) {
      this.setState({isLoading: false});
      this.props.navigation.navigate('App');
    }
    if (!prevProps.authError && this.props.authError) {
      this.setState({isLoading: false});
    }
  };

  navToLanding = () => this.props.navigation.navigate('Landing');
  navToSignUp = () => this.props.navigation.navigate('SignUpOne');

  loginAndNavToApp = () => {
    this.setState({isLoading: true});
    const {usernameValue, passwordValue, homeserverValue} = this.state;
    if (homeserverValue.length === 0) {
      this.props.createMatrixClient(usernameValue, passwordValue);
    } else {
      this.props.createMatrixClient(
        usernameValue,
        passwordValue,
        `https://${homeserverValue}`
      );
    }
  };

  updateInputValues = (key, value) => {
    this.props.clearAuthError();
    this.setState({[key]: value});
  };

  render() {
    const {authError} = this.props;
    const {
      usernameValue,
      passwordValue,
      homeserverValue,
      isLoading
    } = this.state;

    const placeholderTextColor = Color(COLORS.gray.one)
      .alpha(0.2)
      .rgb()
      .string();

    return (
      <PageWrapper>
        <TopBlob source={topBlob} />
        <BottomBlob source={bottomBlob} />
        <BackButton onPress={this.navToLanding}>
          <BackIcon />
        </BackButton>
        <Title>Welcome back!</Title>

        <PageMargin keyboardShouldPersistTaps='handled'>
          <SignUpInput
            value={usernameValue}
            onChangeText={text => this.updateInputValues('usernameValue', text)}
            placeholder='Username'
            placeholderTextColor={placeholderTextColor}
            returnKeyType='next'
            onSubmitEditing={() => this.passwordInput.focus()}
            autoCapitalize='none'
          />
          <SignUpInput
            ref={ref => (this.passwordInput = ref)}
            secureTextEntry
            value={passwordValue}
            onChangeText={text => this.updateInputValues('passwordValue', text)}
            placeholder='Password'
            placeholderTextColor={placeholderTextColor}
            autoCapitalize='none'
          />
          <InputWrapper>
            <HomeserverInput
              value={homeserverValue}
              onChangeText={text =>
                this.updateInputValues('homeserverValue', text)
              }
              placeholder='matrix.org (optional)'
              placeholderTextColor={placeholderTextColor}
              autoCapitalize='none'
              returnKeyType='go'
              onSubmitEditing={this.loginAndNavToApp}
            />
            <FakeHttpText>https://</FakeHttpText>
          </InputWrapper>
          {authError && (
            <ErrorText>
              Looks like something went wrong.{'\n'}Try again!
            </ErrorText>
          )}
          <ButtonWrapper>
            <Button
              title='Login'
              isLoading={isLoading}
              onPress={this.loginAndNavToApp}
            />
          </ButtonWrapper>
          <View style={{height: SCREEN_HEIGHT / 2}} />
        </PageMargin>

        {/* <AuthFooter mainText='Register' mainAction={this.navToSignUp} /> */}
      </PageWrapper>
    );
  }
}

const mapStateToProps = state => ({
  authError: authErrorSelector(state),
  userData: userDataSelector(state)
});

const mapDispatchToProps = {
  clearAuthError,
  createMatrixClient
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

const PageMargin = styled.ScrollView`
  margin-left: 30;
  margin-right: 30;
  padding-top: 50;
`;

const TopBlob = styled(Image)`
  position: absolute;
  top: -285;
  left: -100;
`;

const BottomBlob = styled(Image)`
  position: absolute;
  bottom: -500;
  left: -400;
`;

const BackButton = styled.TouchableOpacity`
  width: 60;
  height: 60;
  justify-content: center;
  align-items: center;
`;

// const BackIcon = styled(DittoIcon)`
//   color: ${({theme}) => theme.dittoWhite};
// `;

const SignUpInput = styled(Input)`
  margin-bottom: 15;
`;

const InputWrapper = styled.View`
  position: relative;
  flex-direction: row;
`;

const HomeserverInput = styled(SignUpInput)`
  padding-left: 93;
  margin-bottom: 0;
  width: 100%;
`;

const FakeHttpText = styled(Text)`
  position: absolute;
  align-self: center;
  margin-left: 15;
`;

const ErrorText = styled(Text)`
  color: ${COLORS.red};
  font-size: 16;
  width: 300;
  text-align: center;
  align-self: center;
  margin-top: 20;
`;

const ButtonWrapper = styled.View`
  width: ${SCREEN_WIDTH};
  justify-content: center;
  align-items: center;
  align-self: center;
  margin-top: 50;
`;

const Title = styled(Text)`
  font-weight: bold;
  font-size: 30;
  margin-top: 30;
  margin-left: 30;
`;
