import React from 'react';
import styled from 'styled-components/native';
import ChatList from '../../../components/home/ChatList';
import {COLORS} from '../../../constants';

const debug = require('debug')('ditto:screen:DirectMessageScreen');

const DirectMessageScreen = () => (
  <Wrapper>
    <ChatList />
  </Wrapper>
);
export default DirectMessageScreen;

const Wrapper = styled.View`
  flex: 1;
  background-color: ${COLORS.dark};
`;
