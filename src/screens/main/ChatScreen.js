import React, {Component} from 'react';
import {
  clientSelector,
  currentMessagesSelector,
  userDataSelector
} from '../../redux/selectors';
import {
  fetchPreviousMessages,
  getAllRoomMembers,
  getRoomTimeline,
  retrieveDisplayNameListFromCache,
  saveDisplayNameListToCache
} from '../../api/matrix';
import {
  formatMatrixEvent,
  setCurrentMessages
} from '../../redux/actions/MatrixActions';

import {COLORS} from '../../constants';
import Composer from '../../components/chat/Composer';
import Icon from 'react-native-vector-icons/Entypo';
import {Image} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import {KeyboardAwareFlatList} from 'react-native-keyboard-aware-scroll-view';
import TimelineRenderer from '../../components/chat/TimelineRenderer';
import {connect} from 'react-redux';
import styled from 'styled-components/native';
import {toImageBuffer} from '../../utilities/Misc';

const showdown = require('showdown');
const mdConverter = new showdown.Converter();

const debug = require('debug')('ditto:screens:ChatScreen');

class ChatScreen extends Component {
  static navigationOptions = ({navigation: nav}) => {
    const room = nav.getParam('room', 'Room');
    debug('room', room);
    return {
      title: room.name,
      headerTitleStyle: {
        color: COLORS.gray.one
      },
      headerLeft: <BackButton onPress={() => nav.goBack()} />,
      headerRight: null
    };
  };

  chatScrollView = null;

  state = {
    homeserver: '',
    messages: [],
    lastToken: '',
    displayNameList: []
  };

  onSend = messageText => {
    const html = mdConverter.makeHtml(messageText);
    var content = {
      body: messageText,
      format: 'org.matrix.custom.html',
      formatted_body: html,
      msgtype: 'm.text'
    };

    this.sendTempMessage({
      content,
      type: 'm.room.message'
    });

    const currentRoom = this.props.navigation.getParam('room', null);
    this.props.client
      .sendEvent(currentRoom.roomId, 'm.room.message', content, '')
      .then(res => {
        debug('Message sent successfully', res);
      })
      .catch(err => {
        debug('Message could not be send: ', err);
      });
  };

  onImagePick = () => {
    try {
      debug('onImagePick');
      ImagePicker.showImagePicker(async response => {
        const mxcUrl = await this.props.client.uploadContent(
          toImageBuffer(response.data),
          {
            onlyContentUri: true,
            name: response.fileName,
            type: response.type
          }
        );
        const currentRoom = this.props.navigation.getParam('room', null);

        var content = {
          url: mxcUrl,
          msgtype: 'm.image'
        };
        this.sendTempMessage({
          content,
          type: 'm.room.message'
        });

        await this.props.client.sendImageMessage(
          currentRoom.roomId,
          mxcUrl,
          {
            w: response.width,
            h: response.height,
            mimetype: response.type,
            size: response.fileSize
          },
          response.fileName
        );
        debug('onImagePick successful image send');
      });
    } catch (e) {
      debug('onImagePick error', e);
    }
  };

  sendTempMessage = message => {
    const _id = Math.random() * 10000;
    const newMsg = {
      ...message,
      isTempMessage: true,
      _id,
      event_id: _id,
      sender: this.props.userData.userId
    };
    this.props.setCurrentMessages([newMsg, ...this.props.messages]);
  };

  loadEarlierMessages = async () => {
    try {
      const room = this.props.navigation.getParam('room', null);
      const response = await fetchPreviousMessages(
        room.roomId,
        this.state.lastToken
      );
      // debug('response', response);
      this.setState({lastToken: response.data.end});
      const prevMessages = response.data.chunk.map(msg =>
        formatMatrixEvent(msg)
      );
      // debug('prevMessages', prevMessages);
      this.props.setCurrentMessages([...this.props.messages, ...prevMessages]);
      this.chatScrollView.scrollToEnd();
    } catch (error) {
      debug("Error fetching previous messages, but it's probs fine");
    }
  };

  updateMemberNamesInCache = async members => {
    const nameList = await retrieveDisplayNameListFromCache();
    members.forEach(member => {
      if (!nameList[member.user_id]) {
        nameList[member.user_id] = member.content.displayname;
      }
    });
    saveDisplayNameListToCache(nameList);
    this.setState({displayNameList: nameList});
  };

  //********************************************************************************
  // Lifecycle
  //********************************************************************************

  componentDidMount = async () => {
    const room = this.props.navigation.getParam('room', null);
    debug('room', room);

    const members = await getAllRoomMembers(room.roomId);
    this.updateMemberNamesInCache(members);

    const messages = getRoomTimeline(room);
    debug('messages', messages);
    this.props.setCurrentMessages(messages.reverse());

    const response = await fetchPreviousMessages(room.roomId);
    // debug('last tokENNNN', response);
    this.setState({lastToken: response.data.end});
  };

  componentWillUnmount = () => {
    this.props.setCurrentMessages([]);
  };

  render() {
    // debug('this.props.messages', this.props.messages);
    const {messages} = this.props;
    return (
      <Wrapper behavior='padding' keyboardVerticalOffset={100}>
        <KeyboardAwareFlatList
          extraHeight
          inverted
          data={messages}
          renderItem={({item, index}) => (
            <TimelineRenderer
              message={item}
              displayNames={this.state.displayNameList}
              prevMessage={messages[index + 1]}
              nextMessage={messages[index - 1]}
            />
          )}
          onEndReached={this.loadEarlierMessages}
          onEndReachedThreshold={0.5}
          keyExtractor={item => item.event_id}
        />
        <Composer
          onSend={msg => this.onSend(msg)}
          onImagePick={this.onImagePick}
        />
      </Wrapper>
    );
  }
}
const mapStateToProps = state => ({
  userData: userDataSelector(state),
  client: clientSelector(state),
  messages: currentMessagesSelector(state)
});
const mapDispatchToProps = {
  setCurrentMessages
};
export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);

const BackTouchable = styled.TouchableOpacity``;

const BackButton = ({onPress}) => (
  <BackTouchable onPress={onPress}>
    <Icon
      name='chevron-left'
      size={35}
      color={COLORS.gray.one}
      style={{marginLeft: 4}}
    />
  </BackTouchable>
);

const Wrapper = styled.KeyboardAvoidingView`
  flex: 1;
  background-color: ${COLORS.dark};
`;
