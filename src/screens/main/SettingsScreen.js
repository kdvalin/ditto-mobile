import {Linking, TouchableOpacity, View} from 'react-native';
import React, {useEffect, useState} from 'react';
// Redux
import {clientSelector, userDataSelector} from '../../redux/selectors';

// Constants
import {COLORS} from '../../constants';
// Components
import DittoButton from '../../components/library/Button';
import ImagePicker from 'react-native-image-picker';
import Input from '../../components/library/Input';
import Text from '../../components/library/Text';
import codePush from 'react-native-code-push';
import {connect} from 'react-redux';
import {logoutUser} from '../../redux/actions/MatrixActions';
import styled from 'styled-components/native';
import {withNavigation} from 'react-navigation';
// Helpers
import { toImageBuffer } from '../../utilities/Misc';

const debug = require('debug')('ditto:screen:SettingsScreen');

const SettingsScreen = ({userData, client, logoutUser, navigation}) => {
  const currentDisplayName = userData.displayName;
  const currentAvatarUrl = userData.avatarUrl;
  const [displayName, setDisplayName] = useState(currentDisplayName);
  const [avatarUrl, setAvatarUrl] = useState(currentAvatarUrl);
  const [updateMetadata, setUpdateMetadata] = useState('No Version Data');

  useEffect(() => {
    (async () => {
      const avatarData = await client.getProfileInfo(
        userData.userId,
        'avatar_url'
      );
      const url = client.mxcUrlToHttp(avatarData.avatar_url, 150, 150, 'crop');
      setAvatarUrl(url);
    })();
  });

  useEffect(() => {
    (async function() {
      try {
        debug('codePush.getUpdateMetadata()');
        const codePushMetadata = await codePush.getUpdateMetadata();
        debug('codePush.getUpdateMedatadata response', codePushMetadata);
        setUpdateMetadata(
          `${codePushMetadata.appVersion} - ${codePushMetadata.label}`
        );
      } catch (e) {
        debug('codePush.getUpdateMetadata error', e);
      }
    })();
  });

  const chooseAvatar = () => {
    debug('chooseAvatar');

    const options = {title: 'Choose avatar', mediaType: 'photo'};
    try {
      ImagePicker.showImagePicker(options, async response => {
        debug('client.uploadContent');
        const mxc = await client.uploadContent(
          toImageBuffer(response.data),
          {
            onlyContentUri: true,
            name: response.fileName,
            type: response.type
          }
        );
        debug('client.uploadContent response', mxc);

        await client.setAvatarUrl(mxc);
        const url = client.mxcUrlToHttp(mxc, 150, 150, 'crop');
        debug('chooseAvatar:url ---', url);
        setAvatarUrl(url);
      });
    } catch (e) {
      debug('chooseAvatar error', e);
    }
  };

  const saveDisplayName = async () => {
    debug('save display name');
    if (currentDisplayName !== displayName) {
      try {
        await client.setDisplayName(displayName);
      } catch (e) {
        debug('saveDisplayName', e);
      }
    }
  };

  const logout = async () => {
    debug('logout');
    logoutUser();
    navigation.navigate('Landing');
  };

  return (
    <SettingsWrapper>
      <TouchableOpacity onPress={chooseAvatar}>
        <Avatar source={{uri: avatarUrl}}></Avatar>
      </TouchableOpacity>
      <View>
        <DisplayName
          onChangeText={setDisplayName}
          placeholder='Set display name'
          placeholderTextColor={COLORS.placeholderTextColor}
          value={displayName}
          onBlur={saveDisplayName}
        />
      </View>
      <Spacer />
      {updateMetadata && (
        <>
          <InfoWrapper>
            <Text>{updateMetadata}</Text>
          </InfoWrapper>
          <Spacer />
        </>
      )}
      <InfoWrapper>
        <InfoButton url='https://dittochat.org' text='Website' />
      </InfoWrapper>
      <InfoWrapper>
        <InfoButton url='https://plan.dittochat.org/' text='Request Feature' />
        <InfoLink> / </InfoLink>
        <InfoButton
          url='https://gitlab.com/ditto-chat/ditto-mobile/issues'
          text='Report Bug'
        />
      </InfoWrapper>
      <Spacer />
      <InfoWrapper>
        <TouchableOpacity onPress={logout}>
          <LogoutLink>Logout</LogoutLink>
        </TouchableOpacity>
      </InfoWrapper>
    </SettingsWrapper>
  );
};

const InfoButton = ({url, text}) => {
  const openURL = url => async () => {
    try {
      debug('open website');
      await Linking.openURL(url);
    } catch (err) {
      debug('open website error', err);
    }
  };

  return (
    <TouchableOpacity onPress={openURL(url)}>
      <InfoLink>{text}</InfoLink>
    </TouchableOpacity>
  );
};

const SettingsWrapper = styled.View`
  background-color: ${({theme}) => theme.backgroundColor};
  height: 100%;
  padding-top: 30;
`;

const Avatar = styled.Image`
  border-radius: 80;
  width: 150;
  height: 150;
  align-self: center;
`;

const DisplayName = styled(Input)`
  width: 250;
  align-self: center;
  background-color: ${COLORS.headerColor};
  margin-top: 20;
`;

const InfoWrapper = styled.View`
  background-color: ${COLORS.headerColor};
  padding-top: 12;
  padding-bottom: 12;
  flex-direction: row;
  justify-content: center;
`;

const InfoLink = styled.Text`
  color: ${COLORS.blue};
  font-size: 20;
  text-align: center;
`;

const LogoutLink = styled.Text`
  color: ${COLORS.red};
  text-align: center;
  font-size: 20;
`;

const Spacer = styled.View`
  background-color: ${({theme}) => theme.backgroundColor};
  height: 40;
`;

const mapStateToProps = state => ({
  userData: userDataSelector(state),
  client: clientSelector(state)
});
const mapDispatchToProps = {
  logoutUser
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withNavigation(SettingsScreen));
