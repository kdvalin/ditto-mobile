import { Linking } from 'react-native';
import Color from 'color';

const debug = require('debug')('ditto:utilities:Misc');
global.Buffer = global.Buffer || require('buffer').Buffer;

export const toImageBuffer = data => new Buffer(data, 'base64');

function hashCode(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}

function intToHex(i) {
  const c = (i & 0x00ffffff).toString(16).toUpperCase();
  return '00000'.substring(0, 6 - c.length) + c;
}

export const getNameColor = name => {
  const code = hashCode(name);
  const hex = intToHex(code);
  let col = Color(`#${hex}`);
  if (col.isDark()) {
    col = col.lighten(.7);
  }
  return col.hex();
};

export function isEmoji(str) {
  var ranges = [
    '\ud83c[\udf00-\udfff]', // U+1F300 to U+1F3FF
    '\ud83d[\udc00-\ude4f]', // U+1F400 to U+1F64F
    '\ud83d[\ude80-\udeff]' // U+1F680 to U+1F6FF
  ];
  if (str.match(ranges.join('|'))) {
    if (str.match(/[a-zA-Z0-9\t\s\w]/)) {
      return false;
    }
    return true;
  } else {
    return false;
  }
}

const WWW_URL_PATTERN = /^www\./i;
export const onUrlPress = (url: string) => {
  // When someone sends a message that includes a website address beginning with "www." (omitting the scheme),
  // react-native-parsed-text recognizes it as a valid url, but Linking fails to open due to the missing scheme.
  if (WWW_URL_PATTERN.test(url)) {
    onUrlPress(`http://${url}`)
  } else {
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.error('No handler for URL:', url)
      } else {
        Linking.openURL(url)
      }
    })
  }
}
