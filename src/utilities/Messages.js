const debug = require('debug')('ditto:utilities:Messages');

export const getChatSnippet = room => {
  try {
    const timeline = room.getLiveTimeline().getEvents();

    const lastMessage = timeline[timeline.length - 1];
    const msgContent = lastMessage?.event?.content;

    let snippet = '';

    switch (msgContent?.msgtype) {
      case 'm.text':
        snippet = msgContent?.body;
        break;
      case 'm.image':
        snippet = 'Someone sent an image.';
        break;
      default:
        snippet = 'Something happened!';
        break;
    }

    return snippet;
  } catch (error) {
    // debug('Error getting room timeline', error);
    return '...';
  }
};
