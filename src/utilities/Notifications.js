import { Alert, Clipboard, Platform } from 'react-native';
import PushNotificationIOS from '@react-native-community/push-notification-ios';

export const startupNotifications = () => {
  if (Platform.OS === 'ios') {
    PushNotificationIOS.addEventListener('register', onRegistered);
    PushNotificationIOS.addEventListener(
      'registrationError',
      onRegistrationError,
    );
    PushNotificationIOS.addEventListener(
      'notification',
      onRemoteNotification,
    );
    PushNotificationIOS.addEventListener(
      'localNotification',
      onLocalNotification,
    );

    PushNotificationIOS.requestPermissions();
  }
}

export const shutdownNotifications = () => {
  if (Platform.OS === 'ios') {
    PushNotificationIOS.removeEventListener('register', onRegistered);
    PushNotificationIOS.removeEventListener(
      'registrationError',
      onRegistrationError,
    );
    PushNotificationIOS.removeEventListener(
      'notification',
      onRemoteNotification,
    );
    PushNotificationIOS.removeEventListener(
      'localNotification',
      onLocalNotification,
    );
  }
}

// **********************************************
// Helpers
// **********************************************

const onRegistered = (deviceToken) => {
  Clipboard.setString(deviceToken);
  Alert.alert(
    'Registered For Remote Push',
    `Device Token: ${deviceToken}`,
    [
      {
        text: 'Dismiss',
        onPress: null,
      },
    ],
  );
}

const onRegistrationError = (error) => {
  Alert.alert(
    'Failed To Register For Remote Push',
    `Error (${error.code}): ${error.message}`,
    [
      {
        text: 'Dismiss',
        onPress: null,
      },
    ],
  );
}

const onRemoteNotification = (notification) => {
  const result = `Message: ${notification.getMessage()};\n
    badge: ${notification.getBadgeCount()};\n
    sound: ${notification.getSound()};\n
    category: ${notification.getCategory()};\n
    content-available: ${notification.getContentAvailable()}.`;

  AlertIOS.alert('Push Notification Received', result, [
    {
      text: 'Dismiss',
      onPress: null,
    },
  ]);
}

const onLocalNotification = (notification) => {
  AlertIOS.alert(
    'Local Notification Received',
    'Alert message: ' + notification.getMessage(),
    [
      {
        text: 'Dismiss',
        onPress: null,
      },
    ],
  );
}